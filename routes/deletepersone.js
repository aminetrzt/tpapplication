var express = require('express');
var router = express.Router();
var fs = require('fs');
var bodyParser = require('body-parser')
var urlencodedParser = bodyParser.urlencoded({ extended: false })

router.get('/', function(req, res, next) {
  res.render('deletepersone');
})


router.post('/', urlencodedParser, function (req, res, next) {
  console.log(req.body);
 
let filename = req.body.id+".json"
 
fs.unlinkSync(filename)

res.render("deletepersone" )
})



module.exports = router;