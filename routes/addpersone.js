var express = require('express');
var router = express.Router();
var fs = require('fs');

var bodyParser = require('body-parser') 

var urlencodedParser = bodyParser.urlencoded({ extended: false })

router.get('/', function(req, res) {
  res.render('addpersone');
})


router.post('/', urlencodedParser, function (req, res) {
 
  let per = {};

  per.nom = req.body.nom
  per.email = req.body.email
  per.age = req.body.age
  per.prenom = req.body.prenom
  per.adresse = req.body.adresse
  per.id = req.body.id
  
let nomf = per.id+".json"
 
let data = JSON.stringify(per);
fs.writeFileSync( nomf, data);
res.render("addpersone" )
})



module.exports = router;